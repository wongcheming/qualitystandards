
Quality statement 1: Diagnosis
------------------------------

### Quality statement

People with COPD have one or more indicative symptoms recorded, and have
the diagnosis confirmed by post-bronchodilator spirometry carried out on
calibrated equipment by healthcare professionals competent in its
performance and interpretation.

### Quality measure

**Structure:**

​a) Evidence of local arrangements to ensure that clinical diagnoses of
COPD include a record of one or more indicative symptoms.

​b) Evidence of local arrangements to ensure that people diagnosed with
COPD have the diagnosis confirmed by post-bronchodilator spirometry.

​c) Evidence of local arrangements to ensure that post-bronchodilator
spirometry is carried out on correctly calibrated equipment.

​d) Evidence of local arrangements to ensure that those carrying out
post-bronchodilator spirometry are competent in its performance and
interpretation.

**Process:**

​a) Proportion of people with COPD who have one or more indicative
symptoms recorded.

Numerator – the number of people in the denominator with one or more
indicative symptoms recorded.

Denominator – the number of people with COPD.

​b) Proportion of people with COPD who have the diagnosis confirmed by
post-bronchodilator spirometry.

Numerator – the number of people in the denominator who have
confirmatory post-bronchodilator spirometry.

Denominator – the number of people with COPD.

### What the quality statement means for each audience

**Service providers**ensure diagnoses of COPD include a record of one or
more indicative symptoms, and are confirmed by post-bronchodilator
spirometry carried out on calibrated equipment by healthcare
professionals competent in its performance and interpretation.

**Healthcare professionals**ensure that people diagnosed with COPD have
a record of one or more indicative symptoms and confirmatory
post-bronchodilator spirometry. Those carrying out spirometry ensure
that the equipment is calibrated and that they are competent in its
performance and interpretation.

**Commissioners**ensure they commission services that record one or more
indicative symptoms when diagnosing COPD, and confirm diagnoses of COPD
with post-bronchodilator spirometry carried out on calibrated equipment
by healthcare professionals competent in its performance and
interpretation.

**People with COPD**are identified by having at least one symptom of
COPD (such as breathlessness, long-lasting cough or often coughing up
phlegm) and have their diagnosis confirmed by a trained healthcare
professional using specialist equipment to test how well the lungs work.

### Source guidance

[NICE clinical guideline 101](/guidance/cg101) recommendations 1.1.1.1
(key priority for implementation), 1.1.1.2, 1.1.2.2 and 1.1.2.4 (key
priority for implementation).

### Data source

**Structure:** a) to d) Local data collection.

**Process:**

​a) Local data collection.

​b) The percentage of all patients with COPD diagnosed after 1 April
2008 in whom the diagnosis has been confirmed by post-bronchodilator
spirometry. Available from Quality and Outcomes Framework indicator
[COPD 12](https://mqi.ic.nhs.uk/Search.aspx?query=copd&ref=1.09.03.01).

### Definitions

Indicative symptoms include but are not limited to:

-   exertional breathlessness

-   chronic cough

-   regular sputum production

-   frequent winter 'bronchitis'.
